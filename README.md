# Vpliv "imbalances" na ostrino pozicije

Ostrino pozicije bom definiral subjektivno, kar pomeni,
da ni odvisna zgolj od pozicije, ampak tudi od igralcev.
Definiral jo bom kot število odločenih iger, ki so prišle iz
te pozicije. To se namreč ujema z intuicijo o tem, kdaj
je pozicija ostra. Kriterij ostrine, ki bi analiziral
pozicijo samo bi bil tako računalniško preintenziven kot
še bolj nezanesljiv (nekatere pozicije imajo na voljo le eno
potezo, ki preživi, pa niso ostre - namreč ta poteza je dovolj
očitna).

Analiziral bom partije vzete iz:
- Caissabase 13.10.2018 (http://caissabase.co.uk/)

Iz vsake bom zajel prvo pozicijo po otvoritvi. Za konec otvoritve
uporabljam modificiran Lichessov kriterij.
(https://github.com/ornicar/scalachess/blob/master/src/main/scala/Divider.scala).

Otvoritev se konča, če je izpolnjeno eno od:
* Prišlo je do dveh menjav figur (ne kmetov)
* Beli ali črni imata manj ko 4 figure nerazvite (na zadnji vrsti)

Za to pozicijo in partijo bom zajel:
* Header podatke (kdo je igral, kje, kdaj)
* Kmečko strukturo
* Material
* FEN pozicije
* Vse poteze v partiji

Iz zajetih podatkov bom v analizi pridobil značilnosti pozicije, npr.
prisotnost podvojenih kmetov ali izoliranega kmeta, stran rušade.

Delovne hipoteze:

* Več ko je razlik med stranema (v kmečki strukturi, materialu, prostoru),
bolj je pozicija ostra v večini primerov.
* Romantične gambitne linije so z razvojem šaha izgubile zobe in ostrino.

Aktualni podatki so zajeti iz datoteke caissabase_20up.pgn, ki vsebuje vse
partije v Caissabase 13.10.2018, ki so daljše od 20 potez. Zajeti podatki
so v datoteki caissabase_20up.csv.
