import chess.pgn as cpgn
import chess
import re

def read_games(pgn):
    """Read all games from pgn and return them as python-chess game files in array."""

    games = [cpgn.read_game(pgn)]
    while games[-1]:  # Reading beyond EOF returns falsey value.
        games.append(cpgn.read_game(pgn))

    return games[:-1]


def read_n_games(pgn, n):
    """Read next n games from pgn."""
    games = []

    for i in range(n):
        game = cpgn.read_game(pgn)
        if game:
            games.append(game)

    return games

def extract_fens(game):
    """Return array of all different FEN strings in game."""

    fens = []
    board = game.board()
    for move in game.main_line():
        board.push(move)
        fens.append(board.fen())

    return fens


def clean_fen(fen):
    """Removes metadata (castling rights, en passants, etc.) from fen."""

    return fen.split(" ")[0]


def material_in_fen(fen):
    """Counts material in position for black and white.

Returns a dictionary with keys "P", "N", "R", "Q", "B", "K" (white pieces)
and "p", "n", "r", "q", "b", "k" (black pieces)."""

    cleaned_fen = clean_fen(fen)
    count_dict = {}
    for key in ["P", "N", "B", "R", "Q", "K", "p", "n", "b", "r", "q", "k"]:
        count_dict[key] = cleaned_fen.count(key)

    return count_dict


def detailed_material_in_fen(fen):
    """Like material in fen, it just treats light squared and dark squared bishops
as different pieces, naming them: LSB, DSB (light white, dark white) and lsb, dsb (light black, dark black)."""
    count_dict = material_in_fen(fen)
    count_dict.pop("b")
    count_dict.pop("B")
    
    board = chess.BaseBoard(board_fen=clean_fen(fen))
    wbishops = list(board.pieces(chess.BISHOP, chess.WHITE))
    bbishops = list(board.pieces(chess.BISHOP, chess.BLACK))
    
    count_dict["LSB"] = 0
    count_dict["DSB"] = 0
    count_dict["lsb"] = 0
    count_dict["dsb"] = 0
    
    for square in wbishops:
        if (square % 2):
            count_dict["DSB"] += 1
        else:
            count_dict["LSB"] += 1
            
    for square in bbishops:
        if(square % 2):
            count_dict["dsb"] += 1
        else:
            count_dict["lsb"] += 1
    
    return count_dict


def pawn_structure(fen):
    """Return fen with all (non-pawn) pieces removed."""

    structure = ""
    cleaned_fen = clean_fen(fen)
    rows = cleaned_fen.split("/")
    structural_rows = []

    for row in rows:
        structural_row = ""
        count = 0

        for char in row:
            if char.isdigit():  # Digits encode empty squares in FENs.
                count += int(char)
            # If char is piece replace it with empty space.
            elif (not (char in ["p", "P"])):
                count += 1
            else:  # Char is pawn.
                add = ""
                if count != 0:
                    add = str(count)

                count = 0
                structural_row += (add + char)

        if count != 0:
            structural_row += str(count)

        if structural_row == "":
            # Empty row is of course made of 8 empty squares.
            structural_row = "8"

        structural_rows.append(structural_row)

    for row in structural_rows:
        structure += (row + "/")

    return structure[:-1]


def remove_noncentral_pawns(fen):
    """Return fen with noncentral pawns removed."""
    board = chess.BaseBoard(fen)

    a_file = [chess.A1, chess.A2, chess.A3, chess.A4,
              chess.A5, chess.A6, chess.A7, chess.A8]
    b_file = [chess.B1, chess.B2, chess.B3, chess.B4,
              chess.B5, chess.B6, chess.B7, chess.B8]
    g_file = [chess.G1, chess.G2, chess.G3, chess.G4,
              chess.G5, chess.G6, chess.G7, chess.G8]
    h_file = [chess.H1, chess.H2, chess.H3, chess.H4,
              chess.H5, chess.H6, chess.H7, chess.H8]

    for square in (a_file + b_file + g_file + h_file):
        if board.piece_type_at(square) == chess.PAWN:
            board.remove_piece_at(square)

    return board.board_fen()


def print_fen(fen):
    """Print fen row by row."""
    rows = (fen.split(" ")[0]).split("/")
    display = []

    for row in rows:
        for char in row:
            if char.isdigit():
                row = row.replace(char, int(char) * ".")

        display.append(row)

    for row in display:
        print(row)


def is_opening(fen):
    """Return true if fen position is opening like."""
    return (not (is_midgame(fen) or is_endgame(fen)))


def is_midgame(fen):
    """Return true if fen position is midgameish."""
    clean = clean_fen(fen)
    
    if is_endgame(fen):
        return False
    
    # If two piece exchanges occured
    if (len(re.findall("[bBnNqQrR]", clean)) <= 10):
        return True
    
    # If at least one backrank is getting empty (less than 4 pieces of colour on backrank).
    black_backrank = len(re.findall("[bnqrk]", clean[:8]))
    white_backrank = len(re.findall("[BNQRK]", clean[-8:]))
    if (white_backrank < 4) or (black_backrank < 4):
        return True
    
    return False
    

def is_endgame(fen):
    """Return true if fen position is endgameish."""
    clean = clean_fen(fen)
    pieces = len(re.findall("[bBnNqQrR]", clean))
    
    return (pieces < 7)
    
    
def first_midgame_position(game):
    """Return (fen, ply) of first midgame position."""
    board = game.board()
    ply = 0
    for move in game.main_line():
        board.push(move)
        fen = board.fen()
        ply += 1
        
        if is_midgame(fen):
            return (fen, ply)
