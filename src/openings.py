import pandas as pd

df = pd.read_csv("caissabase_20up.csv").drop(["Unnamed: 0"], axis=1)

# Clean empty rows
df.dropna(axis=0, how='all', inplace=True)

# Players above 2200, that is eligible for CM and higher titles.
highrated_white = df['WhiteElo'] > 2200
highrated_black = df['BlackElo'] > 2200

df_highrated = df[highrated_white & highrated_black]

# We will map dates to years
df_highrated['Date'] = df_highrated['Date'].map(
    lambda date: int(date[:4]))  # All dates have same format, so we can cheat.
# Cheat is neccesary since dateutil doesn't recognize this: 2014.??.??

# Now we groupby with ECO code.
eco_groups = df_highrated.groupby('ECO')

# For each eco code we are going to generate a df where columns
# are % of white and black victories, draws and total num of games
# in this year, and indexes are years.


def worker(dataframe):
    print(dataframe.columns)

    return result


for eco_code, group in eco_groups:
    print(eco_code)

    year_groups = group.groupby('Date')
    result = pd.DataFrame(columns=['1-0', '1/2-1/2', '0-1', 'Num'])

    for year, ygroup in year_groups:
        results = ygroup.groupby('Result')
        total = ygroup.size

        white_win, draw, black_win = 0, 0, 0
        # Sometimes there is no draws, sometimes there is no wins.
        try:
            white_win = results.get_group('1-0').size
        except KeyError:
            pass

        try:
            draw = results.get_group('1/2-1/2').size
        except KeyError:
            pass

        try:
            black_win = results.get_group('0-1').size
        except KeyError:
            pass

        result.loc[year] = [white_win / total,
                            draw / total,
                            black_win / total,
                            total / len(ygroup.columns)]

    result.to_csv("openings/" + eco_code + ".csv")
