import welmish_chess as wch
import pandas as pd
import numpy as np

from multiprocessing import Pool
import sys

# All data passed to Pool.map must be serialized, which quickly overruns default recursion limit.
sys.setrecursionlimit(50000)

# How many games are loaded in a time:
STEP = 1000

in_file = sys.argv[1]
out_dir = sys.argv[2]

pgn = open(in_file)


def worker(game):
    """Basic worker function."""
    pair = wch.first_midgame_position(game)

    if (game and pair):
        row = dict(game.headers)
        (fen, ply) = pair

        row["ply"] = ply
        row["fen"] = fen
        game.headers = {}
        row["moves"] = game
        row["pawn structure"] = wch.pawn_structure(fen)
        row["central pawn structure"] = wch.remove_noncentral_pawns(
            row["pawn structure"])

        row.update(wch.detailed_material_in_fen(fen))

        return row

    return {}


games = wch.read_n_games(pgn, STEP)
i = 1

while (games != []):
    print("Iteration:", i)
    pool = Pool(16)
    rows = pool.map(worker, games)
    pool.close()
    pool.join()
    del(pool)

    if rows:
        row_df = pd.DataFrame(data=rows)
        with open(out_dir + "temp" + str(i) + ".csv", 'x') as f:
            try:
                row_df.to_csv(f, header=True)
            except Exception:
                pass

        del(row_df)
        del(rows)

    games = wch.read_n_games(pgn, STEP)
    i += 1
