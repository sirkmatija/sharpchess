import pandas as pd

filenames = ["temp/temp" + str(num) + ".csv" for num in range(1, 3497)]
columns = ["Black", "BlackElo", "BlackTitle",
           "White", "WhiteElo", "WhiteTitle",
           "Date", "ECO", "Event", "Site", "Result",
           "K", "Q", "R", "LSB", "DSB", "N", "P",
           "k", "q", "r", "lsb", "dsb", "n", "p",
           "central pawn structure", "pawn structure",
           "fen", "moves", "ply"]

dataframes = map((lambda name: pd.read_csv(
    name).filter(columns, axis=1)), filenames)
df = pd.concat(dataframes, ignore_index=True)
df.to_csv("caissabase_20up.csv")
